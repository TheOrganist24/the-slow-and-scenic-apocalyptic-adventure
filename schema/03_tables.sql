CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.quests
(
  name TEXT PRIMARY KEY,
  summary TEXT
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.settlements
(
  location TEXT PRIMARY KEY,
  geom geometry
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.legs
(
  track INT PRIMARY KEY REFERENCES tracks.tracks(ogc_fid),
  quest TEXT NOT NULL REFERENCES the_slow_and_scenic_apocalyptic_adventure.quests(name),
  starting_location TEXT NOT NULL REFERENCES the_slow_and_scenic_apocalyptic_adventure.settlements(location),
  ending_location TEXT NOT NULL REFERENCES the_slow_and_scenic_apocalyptic_adventure.settlements(location),
  weather TEXT NOT NULL,
  terrain TEXT NOT NULL,
  swim_or_paddle TEXT,
  avoided_pasties TEXT
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.churches
(
  name TEXT,
  settlement TEXT,
  leg INT REFERENCES the_slow_and_scenic_apocalyptic_adventure.legs(track),
  PRIMARY KEY (name, settlement, leg)
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.company
(
  surname TEXT,
  forename TEXT,
  leg INT REFERENCES the_slow_and_scenic_apocalyptic_adventure.legs(track),
  PRIMARY KEY (surname, forename, leg)
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.memorable_incidents
(
  description TEXT,
  leg INT REFERENCES the_slow_and_scenic_apocalyptic_adventure.legs(track),
  PRIMARY KEY (description, leg)
);

CREATE TABLE the_slow_and_scenic_apocalyptic_adventure.upcoming_hikes
(
  day DATE PRIMARY KEY,
  starting VARCHAR NOT NULL REFERENCES the_slow_and_apocalyptic_adventure.settlements(location),
  ending VARCHAR NOT NULL REFERENCES the_slow_and_apocalyptic_adventure.settlements(location),
  terrain VARCHAR NOT NULL,
  plan geometry
);
