CREATE OR REPLACE VIEW the_slow_and_scenic_apocalyptic_adventure.quests_summary
AS
SELECT processed.name,
  processed.summary,
  count(processed.track) AS tracks,
  min(starting_point) AS starting_point,
  min(finishing_point) AS finishing_point,
  sum(processed.length) AS length,
  json_agg(json_build_object('name', leg_name, 'id', track)) AS legs
FROM (
  SELECT quests.name,
    quests.summary,
    legs.track,
    starting_location || ' to ' || ending_location AS leg_name,
    FIRST_VALUE(starting_location) OVER (
      PARTITION BY quests.name
      ORDER BY legs.track
    ) AS starting_point,
    LAST_VALUE(ending_location) OVER (
      PARTITION BY quests.name
      ORDER BY legs.track
        RANGE BETWEEN UNBOUNDED PRECEDING
          AND UNBOUNDED FOLLOWING
    ) AS finishing_point,
    st_length(tracks.wkb_geometry::geography, false) * 0.0006213712::double precision AS length
  FROM the_slow_and_scenic_apocalyptic_adventure.quests
  JOIN the_slow_and_scenic_apocalyptic_adventure.legs
    ON legs.quest = quests.name
  JOIN tracks.tracks
    ON legs.track = tracks.ogc_fid
) AS processed
GROUP BY processed.name,
  processed.summary
ORDER BY min(processed.track)
;


CREATE OR REPLACE VIEW the_slow_and_scenic_apocalyptic_adventure.adventure_geometry
AS
SELECT ST_Y(ST_AsText(ST_Centroid(ST_Collect(tracks.wkb_geometry)))) AS latitude,
  ST_X(ST_AsText(ST_Centroid(ST_Collect(tracks.wkb_geometry)))) AS longitude,
  ST_AsGeoJSON(ST_Collect(tracks.wkb_geometry)) as geom
FROM the_slow_and_scenic_apocalyptic_adventure.legs
JOIN tracks.tracks
  ON legs.track = tracks.ogc_fid;


CREATE OR REPLACE VIEW the_slow_and_scenic_apocalyptic_adventure.leg_summary
AS
SELECT legs.track AS leg,
  logged.track AS start_time,
  starting_location,
  ending_location,
  weather,
  terrain,
  swim_or_paddle,
  avoided_pasties,
  ST_Length(tracks.wkb_geometry::geography, false) * 0.0006213712::double precision AS length,
  ST_AsGeoJSON(tracks.wkb_geometry) as geom,
  ST_X(ST_Centroid(tracks.wkb_geometry)) AS longitude,
  ST_Y(ST_Centroid(tracks.wkb_geometry)) AS latitude,
  churches.churches,
  company.company,
  incidents.incidents
FROM the_slow_and_scenic_apocalyptic_adventure.legs
JOIN tracks.tracks
  ON legs.track = tracks.ogc_fid
JOIN tracks.logged
  ON legs.track = logged.id
FULL OUTER JOIN (
  SELECT leg,
  json_agg(
    json_build_object(
        'church', churches.name,
        'settlement', churches.settlement
    )
  ) AS churches
  FROM the_slow_and_scenic_apocalyptic_adventure.churches
  GROUP BY leg ) AS churches
  ON churches.leg = legs.track
FULL OUTER JOIN (
  SELECT leg,
  json_agg(
    json_build_object(
        'surname', company.surname,
        'forename', company.forename
    )
  ) AS company
  FROM the_slow_and_scenic_apocalyptic_adventure.company
  GROUP BY leg ) AS company
  ON company.leg = legs.track
LEFT OUTER JOIN (
  SELECT leg,
  string_agg(description, ' ') AS incidents
  FROM the_slow_and_scenic_apocalyptic_adventure.memorable_incidents
  GROUP BY leg ) AS incidents
  ON incidents.leg = legs.track
;
