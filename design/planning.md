# Planning Meeting
> 2023-10-28

## Look and feel
Two pages
* Overview of whole route
* overview of leg

Need a sentance summary for each "quest".

## Things to include
* People - Who joined us - LIST
* Length - CALC
* Map of the route - CALC
* Weather - TEXT
* Terrain - TEXT
* Avoided pasties - LIST
* Swim/paddle y/n - TEXT
* Memorable incidents - LIST
* Church watch - LIST
* Date - CALC
* Accomodation - LIST
* Transport - NO
* Photos

## Legs and quests so far
### Princetown to Ivybridge
> Feasbility study

* Dry, cold, overcast
* Bog, moorland, puffing bill track
* Homemade sandwiches from the hotel
* Bog paddle
* Foxtor mire
* Royal Duchee Hotel

### Ivybridge to Exeter
* SW families
* Four households way

- IVY to TOT
  * Wet
  * Unbelievably "flat", field paths and country lanes
  * Sausage roll (from Warren's Bakery), pub
  * Remarkable flatness and the "Fab Beths"
  * Ugborough, North Huish, Diptford
- TOT to PAI
  * Hot
  * Footpaths, country lanes, roads and nature reseves, (rural to urban)
  * Immediate sausage roll avoidance from Totnes Highstreet
  * Notable swim at Goodrington
  * Diversion to Stoke Gabriele
- PAI to NTA
  * Hot
  * Beach, suburbia, country park, suburban country paths/lanes
  * Sarnies in Cockington
  * Paddle at start
  * Avoided the Maltings Taphouse
  * The impassable path
  * Kingskerswell Church
- NTA to TEI
  * Joined by Allens and Megan
  * Hot
  * Estuary path, country lane, footpaths and pavements
  * Greasy spoon cafe
  * Swim at Teignmouth
  * Bishopsteington Methodist Church
- TEI to STR
  * Due to rain, but grew warm and muggy by the end
  * Coastpath, exe estuary trail, estuary
  * Teign-bean cafe
  * The excellent Exe valley trail
- STR to EXE
  * Matt Settle and James McBean
  * Unexpectedly sunny by the end
  * Estuary trail, riverside trail
  * Teign-bean, two pubs
  * Station spotting pro
  * Powderham Church

### Exeter to Cheltenham
- EXE to TIV


