# Metrics
> Improving metrics displayed, and for planning purposes

## Walking Speed
Walking velocity is the speed of travel (m/s) calculated over a whole day's hike.

This is calculated using total distance travelled against total time taken. This therefore includes breaks for rest and avoided pasties.

* `geospatial_tracking.tracks`: LINESTRING
* `geospatial_tracking.hikes`: date
* `geospatial_tracking.track_points`: POINT w/ time
* `the_slow_and_scenic_apocalyptic_adventure.legs`: date, `geospatial_tracking.tracks(ogc_fid)->id`

1. JOIN `.legs` to `.track_points` ON time WITHIN date to retrieve _duration_
2. Extract _distance_ from `.legs`
2. Use _duration_ and _distance_ to calculate _walking speed_

This _walking speed_ can be used to predict hike durations, and to generate `.ics` files.


## Predicted Total Distance and Time
Predicted total distance is the estimation of how far we'll eventually end up walking. Predicted total time should provide the ETA.

I feel that there maybe interesting maths here surrounding fractal measuring (see "Coastline Paradox"). Data I have available include total distance covered (per leg and overall), and _leg speed_. Data potentially available includes distance-as-the-crow-flies between each routing point, and overall. _Leg speed_ is the speed (m/s) including rest days. This will be ridiculously slow but will provide the overall ETA. 

Possible models to consider when calculating from crow-flies distance are:
* Fractal (Coastlines paradox)
* River meanders
* Random walk
* General network analysis


## Maths Page Design

|                   | Covered, Predicted Covered, Accuracy, Predicted Remainder, Same with Fitting to Known Points   |
| Princetown to...  | Crow-Flies          | Crow-Flies Ratio | River Meander | Random Walk | Another Model | Another |
| ----------------- | ------------------- | ---------------- | ------------- | ----------- | ------------- | ------- |
| Taunton           | 130, 50, -60%, 1000 | etc...           |               |             |               |         |
| Bridgwater        |                     |                  |               |             |               |         |
| Highbridge        |                     |                  |               |             |               |         |

## Crow Flies
Explanation...

## Next Model
Etc...
