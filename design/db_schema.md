# Database Schema

The following tables are stored in the `the_slow_and_scenic_apocalyptic_adventure` schema.

```mermaid
erDiagram
    QUEST {
        TEXT name PK
        TEXT summary
    }
    LEG {
        INT track PK, FK
        TEXT quest FK "NOT NULL"
        DATE date "NOT NULL"
        TEXT start FK "NOT NULL"
        TEXT end FK "NOT NULL"
        TEXT weather "NOT NULL"
        TEXT terrain "NOT NULL"
        TEXT swim_or_paddle
        TEXT avoided_pasties
    }
    LEG ||--|| TRACKS : refs
    LEG }|--|| QUEST : refs
    SETTLEMENTS {
        TEXT location PK
    }
    LEG }|--|| SETTLEMENTS : refs
    MEMORABLE_INCIDENTS {
        TEXT description PK
        INT leg FK "NOT NULL"
    }
    MEMORABLE_INCIDENTS }|--|| LEG : refs
    PHOTOS {
        TEXT url PK
        INT leg FK "NOT NULL"
    }
    PHOTOS }|--|| LEG : refs
    COMPANY {
        TEXT surname PK
        TEXT forename PK
        INT leg PK, FK
    }
    COMPANY }|--|| LEG : refs
    CHURCHES {
        TEXT name PK
        TEXT settlement PK, FK
        INT leg PK, FK
    }
    CHURCHES }|--|| SETTLEMENTS : refs
    CHURCHES }|--|| LEG : refs
```
