SYSTEM=the_slow_and_scenic_apocalyptic_adventure
CONFIG_PATH=/etc

.PHONY: settings
settings: $(CONFIG_PATH)/$(SYSTEM)

$(CONFIG_PATH)/$(SYSTEM):
	cp --no-clobber settings.conf $(CONFIG_PATH)/$(SYSTEM)
	$(info Ensure your settings file is up-to-date `$(CONFIG_PATH)/$(SYSTEM)`.)

/srv/$(SYSTEM):
	mkdir /srv/$(SYSTEM)

.PHONY: user
user: /srv/$(SYSTEM)
	useradd tsasaa --home-dir /srv/$(SYSTEM)
	chown tsasaa /srv/$(SYSTEM) --recursive

.PHONY: uninstall
uninstall:
	userdel tsasaa
