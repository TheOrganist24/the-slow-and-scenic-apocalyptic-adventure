# The Slow and Scenic Apocalyptic Adventure
> System for managing and sharing information about the hike from Ivybridge to Durham

![System Design](design/system_design.drawio.png)


## Components
* **[Schema](schema/README.md)**: The schema for creating the website
* **[Website](website/README.md)**: The code for the [blog](https://nature.theorganist24.co.uk/the-slow-and-scenic-apocalyptic-adventure)
* **[Planning Service](planning_service/README.md)**: The tool to create hike tracks and decide on details
* **[Route Exporter](route_exporter/README.md)**: The automation to move planned hikes to a mobile device


## Preliminary Setup
To run the above components, first create a user and the config file:

```bash
sudo make user
sudo make settings
```
