"""DB calls for retrieving data."""

import os
from typing import List, Tuple

import psycopg2


def retrieve_quests():
    """Retrieve quest data from DB."""
    connection = psycopg2.connect(
        database=os.environ["DATABASE"],
        host=os.environ["DATABASE_HOST"],
        user=os.environ["DATABASE_USER"],
        password=os.environ["DATABASE_PASSWORD"],
    )
    query = """SELECT *
FROM the_slow_and_scenic_apocalyptic_adventure.quests_summary
;
"""

    with connection.cursor() as cursor:
        cursor.execute(query)
        quests = cursor.fetchall()

    connection.close()

    return quests


def retrieve_adventure_geometry():
    """Retrieve quest data from DB."""
    connection = psycopg2.connect(
        database=os.environ["DATABASE"],
        host=os.environ["DATABASE_HOST"],
        user=os.environ["DATABASE_USER"],
        password=os.environ["DATABASE_PASSWORD"],
    )
    query = """SELECT *
FROM the_slow_and_scenic_apocalyptic_adventure.adventure_geometry
;
"""

    with connection.cursor() as cursor:
        cursor.execute(query)
        summary = cursor.fetchone()

    connection.close()

    if not summary:
        raise Exception

    return summary


def retrieve_leg_summary(id):
    """Retrieve quest data from DB."""
    connection = psycopg2.connect(
        database=os.environ["DATABASE"],
        host=os.environ["DATABASE_HOST"],
        user=os.environ["DATABASE_USER"],
        password=os.environ["DATABASE_PASSWORD"],
    )
    query = """SELECT *
FROM the_slow_and_scenic_apocalyptic_adventure.leg_summary
WHERE leg = %s
;
"""

    with connection.cursor() as cursor:
        cursor.execute(query, (id,))
        summary = cursor.fetchone()

    connection.close()

    if not summary:
        raise Exception

    return summary


def retrieve_total_distance():
    """Retrieve quest data from DB."""
    connection = psycopg2.connect(
        database=os.environ["DATABASE"],
        host=os.environ["DATABASE_HOST"],
        user=os.environ["DATABASE_USER"],
        password=os.environ["DATABASE_PASSWORD"],
    )
    query = """SELECT sum(
  ST_Length(
    tracks.wkb_geometry::geography, false
  ) * 0.0006213712::double precision
) AS length
FROM the_slow_and_scenic_apocalyptic_adventure.legs
JOIN geospatial_tracking.tracks
  ON legs.track = tracks.ogc_fid
;
"""

    with connection.cursor() as cursor:
        cursor.execute(query, (id,))
        distance_data = cursor.fetchone()

    connection.close()

    if not distance_data:
        raise Exception

    return distance_data[0]
