"""The main runtime for the adventure blog."""

from datetime import datetime

import folium

from flask import Flask, render_template
from werkzeug.middleware.proxy_fix import ProxyFix

from the_slow_and_scenic_apocalyptic_adventure_website.db_calls import (
    retrieve_quests,
    retrieve_adventure_geometry,
    retrieve_leg_summary,
    retrieve_total_distance,
)
from the_slow_and_scenic_apocalyptic_adventure_website.pages import PageBuilder

app = Flask(__name__)
app.wsgi_app = ProxyFix(  # type: ignore
    app.wsgi_app, x_for=1, x_proto=1, x_host=1, x_prefix=1
)


@app.route("/")
def overview() -> str:
    """Create the overview page."""
    summary = retrieve_adventure_geometry()
    quests = retrieve_quests()
    distance = retrieve_total_distance()

    latitude, longitude = summary[0], summary[1]
    m = folium.Map(location=(latitude, longitude), zoom_start=8)

    folium.GeoJson(
        summary[2],
    ).add_to(m)

    # set the iframe width and height
    m.get_root().width = "100%"
    iframe = m.get_root()._repr_html_()

    return render_template(
        "overview.html",
        quests=quests,
        distance=distance,
        iframe=iframe,
    )


@app.route("/legid/<id>")
def leg_page(id) -> str:
    """Create the leg page."""
    leg = retrieve_leg_summary(id)
    day = datetime.strptime(
        leg[1],
        "%Y-%m-%d_%H-%M-%S",
    ).strftime("%A %d %B %Y")

    longitude, latitude = leg[10], leg[11]
    m = folium.Map(location=(latitude, longitude), zoom_start=9)

    folium.GeoJson(
        leg[9],
    ).add_to(m)

    # set the iframe width and height
    m.get_root().width = "100%"
    iframe = m.get_root()._repr_html_()

    return render_template("leg.html", leg=leg, day=day, iframe=iframe)


@app.route("/upcoming")
def upcoming() -> str:
    """Create the upcoming hikes page."""
    page = PageBuilder(
        template="upcoming_hikes.html",
        queries={
            "upcoming_hikes": """SELECT day,
  ST_Length(
    plan::geography, false
  ) * 0.0006213712::double precision AS length,
  starting,
  ending,
  terrain
FROM the_slow_and_scenic_apocalyptic_adventure.upcoming_hikes
WHERE day >= current_date
ORDER BY day ASC
;
""",
        },
    )
    return render_template(page.template, hikes=page.data["upcoming_hikes"])
