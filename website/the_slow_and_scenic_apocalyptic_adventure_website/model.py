"""Data models."""

import datetime
from collections import namedtuple
from dataclasses import dataclass
from typing import List

from shapely.geometry import LineString, MultiLineString  # type: ignore


@dataclass
class Quest:
    """Quest data storage."""

    title: str
    summary: str
    length: float


@dataclass
class Overview:
    """Overview page data."""

    quests: List[Quest]
    walk: MultiLineString


Termini = namedtuple("Termini", "start end")


class Statistics:
    """Informational data about the walk."""

    termini: Termini
    date: datetime.date
    track: LineString
    length: float

    def __init__(
        self,
        termini: Termini,
        date: datetime.date,
        track: LineString,
    ):
        """Initialiase class and calculate track length."""
        self.termini = termini
        self.date = date
        self.track = track

        self.length = track.length


@dataclass
class Log:
    """Data about things that happened."""

    weather: str
    terrain: str

    swim_or_paddle: str | None = None
    avoided_pasties: str | None = None
    church_watch: List[str] | None = None
    memorable_incidents: List[str] | None = None
    photo_urls: List[str] | None = None
    people: List[str] | None = None
    accomodation: List[str] | None = None


@dataclass
class Leg:
    """Leg page data."""

    statistics: Statistics
    log: Log
