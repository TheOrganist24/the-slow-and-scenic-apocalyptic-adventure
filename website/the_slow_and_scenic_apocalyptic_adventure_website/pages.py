"""Module holding the Page Builder class."""

import os
from pathlib import Path
from typing import Any, Dict

import psycopg2
from psycopg2.extensions import connection as Connection, cursor as Cursor


class PageBuilder:
    """Parent class for building pages."""

    template: str
    data: Dict[str, Any]
    _connection: Connection
    _cursor: Cursor
    _queries: Dict[str, str]

    def __init__(self, template: str, queries: Dict[str, str] = {}):
        """Initialise with database connection and template."""
        template_path: str = f"{os.getcwd()}/the_slow_and_scenic_apocalyptic" \
                             f"_adventure/templates/{template}"
        # if not Path(template_path).is_file():
        #     raise Exception

        self.template = template
        self._queries = queries
        self.data = {}

        self._connection = psycopg2.connect(
            database=os.environ["DATABASE"],
            host=os.environ["DATABASE_HOST"],
            user=os.environ["DATABASE_USER"],
            password=os.environ["DATABASE_PASSWORD"],
        )

        with self._connection.cursor() as self._cursor:
            self._extract_data_from_database()

        self._connection.close()

    def _extract_data_from_database(self) -> None:
        for tag, query in self._queries.items():
            self._cursor.execute(query)
            self.data[tag] = self._cursor.fetchall()
