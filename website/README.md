# The Slow and Scenic Apocalyptic Adventure
> The code for the [blog](https://nature.theorganist24.co.uk/the-slow-and-scenic-apocalyptic-adventure)

## Running
1. Run [preliminary setup](../README.md#Preliminary Setup)
1. Prefix each setting in `/etc/the_slow_and_scenic_apocalyptic_adventure` with `export `
1. Run

```bash
source /etc/the_slow_and_scenic_apocalyptic_adventure
THE_SLOW_AND_SCENIC_APOCALYPTIC_ADVENTURE_PORT=8000 ./startup.sh
```


To deploy using SystemD:

```bash
sudo make install
```
