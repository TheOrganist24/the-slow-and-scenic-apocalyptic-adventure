"""Test module for models."""

from datetime import date

from shapely.geometry import LineString, MultiLineString  # type: ignore
from the_slow_and_scenic_apocalyptic_adventure_website.model import (
    Statistics,
    Termini,
)


def test_when_statistics_model_created_then_length_calculated() -> None:
    test_statistics = Statistics(
        termini=Termini(
            start="Land's End",
            end="John O'Groats",
        ),
        date=date(2022, 11, 19),
        track=LineString([[0, 0], [1, 0], [1, 1]]),
    )

    assert test_statistics.length == 2
