"""Test module for DB calls."""

from unittest.mock import Mock, call, patch

from the_slow_and_scenic_apocalyptic_adventure_website.db_calls import (
    retrieve_quests,
)


@patch("the_slow_and_scenic_apocalyptic_adventure_website.db_calls"
       ".psycopg2.connect")
def test_when_retrieving_quest_data_then_correct_calls_are_made(
    mock_connect: Mock,
) -> None:
    mock_cur: Mock = (
        mock_connect.return_value.cursor.return_value.__enter__.return_value
    )

    mock_cur.fetchall.return_value = [("Test Quest", "A testing quest.", 24.0)]

    retrieve_quests()

    expected_calls = [
        call(
            "SELECT *\n"
            "FROM the_slow_and_scenic_apocalyptic_adventure.quests_summary\n"
            ";\n",
        ),
    ]

    mock_cur.execute.assert_has_calls(expected_calls, any_order=True)
