from unittest.mock import Mock, call, patch

import pytest

from the_slow_and_scenic_apocalyptic_adventure_website.pages import (
    PageBuilder,
)


@patch("the_slow_and_scenic_apocalyptic_adventure_website.pages.psycopg2"
       ".connect")
def test_when_initialised_with_template_path_then_handled(
    mock_connect,
) -> None:
    with pytest.raises(Exception):
        PageBuilder(template="non_existant_template.html")

    PageBuilder(template="base.html")


@patch("pathlib.Path.is_file")
@patch("the_slow_and_scenic_apocalyptic_adventure_website.pages.psycopg2"
       ".connect")
def test_when_provided_with_query_then_called_as_expected(
    mock_connect,
    mock_path_check,
) -> None:
    mock_cur: Mock = (
        mock_connect.return_value.cursor.return_value.__enter__.return_value
    )
    mock_path_check.return_value = True

    PageBuilder(
        template="test.html",
        queries={
          "test": "SELECT 'test';",
        },
    )

    expected_calls = [call("SELECT 'test';")]
    mock_cur.execute.assert_has_calls(expected_calls, any_order=True)


@patch("pathlib.Path.is_file")
@patch("the_slow_and_scenic_apocalyptic_adventure_website.pages.psycopg2"
       ".connect")
def test_when_provided_with_query_then_attribute_populated(
    mock_connect,
    mock_path_check,
) -> None:
    mock_cur: Mock = (
        mock_connect.return_value.cursor.return_value.__enter__.return_value
    )
    mock_cur.fetchall.return_value = [("Test Output")]
    mock_path_check.return_value = True

    page = PageBuilder(
        template="test.html",
        queries={
          "test": "SELECT 'test';",
        },
    )

    assert page.data == {
      "test": [("Test Output")],
    }
