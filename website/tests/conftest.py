"""Extra fixtures for pytest."""

import os
from unittest.mock import patch

import pytest

import the_slow_and_scenic_apocalyptic_adventure_website


@pytest.fixture()
def app():
    """Create app fixture."""
    the_slow_and_scenic_apocalyptic_adventure.app.config.update({
        "TESTING": True,
    })

    yield the_slow_and_scenic_apocalyptic_adventure.app


@pytest.fixture()
def client(app):
    """Create client fixture."""
    return app.test_client()


@pytest.fixture(autouse=True)
def mock_env_vars():
    """Create fixture for mocking environment variables."""
    with patch.dict(
        os.environ,
        {
            "DATABASE": "database",
            "DATABASE_HOST": "db.example.com",
            "DATABASE_USER": "user",
            "DATABASE_PASSWORD": "password",
        },
    ):
        yield

