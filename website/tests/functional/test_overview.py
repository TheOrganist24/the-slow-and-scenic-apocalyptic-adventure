"""Overview page functional testing."""

from unittest.mock import patch

from the_slow_and_scenic_apocalyptic_adventure_website import app


@patch("the_slow_and_scenic_apocalyptic_adventure_website.__main__.folium")
@patch("the_slow_and_scenic_apocalyptic_adventure_website."
       "__main__.retrieve_total_distance")
@patch("the_slow_and_scenic_apocalyptic_adventure_website."
       "__main__.retrieve_quests")
@patch("the_slow_and_scenic_apocalyptic_adventure_website."
       "__main__.retrieve_adventure_geometry")
def test_when_loaded_home_page_returns_200(
    summary,
    quests,
    distance,
    folium,
) -> None:
    with app.test_client() as test_client:
        response = test_client.get("/")
        assert response.status_code == 200


@patch("the_slow_and_scenic_apocalyptic_adventure_website.__main__"
       ".PageBuilder")
def test_when_loaded_next_hikes_page_returns_200(mock_page) -> None:
    mock_page.return_value.template = ("upcoming_hikes.html")
    with app.test_client() as test_client:
        response = test_client.get("/upcoming")
        assert response.status_code == 200
