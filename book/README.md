# Book
> Journal of journey

## Compiling
1. Ensure that LaTeX is available (`texlive-latex-recommended`)
2. `make`

## General Structure
Each chapter should contain:
* Statistics:
  - Weather
  - Terrain
  - Distance covered
* An overview of the route
* Interspersed with conversation topics
* A history of something in the area

Each section should contain a route-map.

It can also contain, notes on developments around the walk, for example:
* New tech pipelines
* Logistical challenges
