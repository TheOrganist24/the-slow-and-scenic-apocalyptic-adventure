"""Test Distance Calculators."""

from shapely.geometry import Point

from distance_calculators import crow_flies


def test_crow_flies_returns_correct_distance() -> None:
    test_start: Point = Point(0, 0)
    test_end: Point = Point(1, 1)
    expected_distance: float = 97.5
    
    assert round(crow_flies(test_start, test_end), 1) == expected_distance
