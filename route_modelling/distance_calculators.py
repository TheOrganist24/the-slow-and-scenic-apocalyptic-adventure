"""This is a module for storing utlities for calculating distance."""

from geopy import distance
from shapely.geometry import Point


def crow_flies(start, end) -> float:
    """I'm making crow_flies function show distance as the crow flies."""
    return distance.distance((start.y, start.x), (end.y, end.x)).miles
