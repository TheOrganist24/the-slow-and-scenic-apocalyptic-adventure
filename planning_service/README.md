# Planning Service
> The tool to create hike tracks and decide on details

A QGIS project to design the routes and add details such as route termini, dates, and terrain summaries.

## Usage
1. Open the project in QGIS
1. Add appropriate settlements
1. Add routes in the `upcoming_hikes` table
