# Route Explorer
> The automation to move planned hikes to a mobile device

## To Deploy
1. Run [preliminary setup](../README.md#Preliminary Setup)
1. Install with `sudo make install`
