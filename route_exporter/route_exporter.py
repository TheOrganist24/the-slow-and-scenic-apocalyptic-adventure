#! /usr/bin/env python3

"""Route exporter."""

import os
import warnings

import geopandas as gpd
import pandas as pd
import psycopg2


def generate_file_name(row: pd.Series) -> str:
    """Create filename from row details."""
    return f"{row['day'].strftime('%Y%m%d')}_" \
           f"{row['starting'].lower().replace(' ', '_')}_" \
           f"to_{row['ending'].lower().replace(' ', '_')}.gpx"


def create_gpx_files(tracks_gdf: gpd.GeoDataFrame) -> None:
    """Export GPX files from GeoDataFrame."""
    FILESHARE = os.environ["TRACKS_DIRECTORY"]

    for index, row in tracks_gdf.iterrows():
        filename: str = generate_file_name(row=row)
        tracks_gdf[tracks_gdf.index == index]["plan"].to_file(
            filename=FILESHARE + filename,
            driver="GPX",
        )


def retrieve_tracks_from_database() -> gpd.GeoDataFrame:
    """Extract tracks data from database."""
    sql: str = """SELECT day,
  starting,
  ending,
  plan
FROM the_slow_and_scenic_apocalyptic_adventure.upcoming_hikes
WHERE day >= current_date
ORDER BY day
;
"""

    with psycopg2.connect(
        database=os.environ["DATABASE"],
        host=os.environ["DATABASE_HOST"],
        user=os.environ["DATABASE_USER"],
        password=os.environ["DATABASE_PASSWORD"],
    ) as connection:
        tracks_gdf: gpd.GeoDataFrame = gpd.GeoDataFrame.from_postgis(
            sql=sql,
            con=connection,
            geom_col="plan",
        )

    connection.close()

    return tracks_gdf


def main() -> None:
    """Run main runtime."""
    create_gpx_files(tracks_gdf=retrieve_tracks_from_database())


if __name__ == "__main__":
    warnings.filterwarnings("ignore")
    main()
