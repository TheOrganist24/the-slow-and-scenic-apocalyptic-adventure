import os
from pathlib import Path
from unittest.mock import patch

import geopandas as gpd
import pandas as pd
import pytest
from shapely.geometry import LineString


from route_exporter import (
    generate_file_name,
    create_gpx_files,
)


@pytest.fixture(autouse=True)
def mock_env_vars():
    """Create fixture for mocking environment variables."""
    with patch.dict(
        os.environ,
        {
            "TRACKS_DIRECTORY": "",
        },
    ):
        yield


test_gdf: gpd.GeoDataFrame = gpd.GeoDataFrame(
    data={
        "day": [
            pd.to_datetime("2000-01-01", format="%Y-%m-%d"),
            pd.to_datetime("2000-01-02", format="%Y-%m-%d"),
        ],
        "starting": [
            "first_start",
            "second_start",
        ],
        "ending": [
            "first_end",
            "second_end",
        ],
        "plan": [
            LineString([[0, 0], [1, 1]]),
            LineString([[2, 2], [3, 3]]),
        ],
    },
    geometry="plan",
)


def test_name_is_generated_correctly() -> None:
    filename: str = generate_file_name(
        row=test_gdf.iloc[0]
    )

    assert filename == "20000101_first_start_to_first_end.gpx"


def test_gpx_files_are_exported() -> None:
    create_gpx_files(tracks_gdf=test_gdf)

    assert Path("20000101_first_start_to_first_end.gpx").is_file()
    assert Path("20000102_second_start_to_second_end.gpx").is_file()


def teardown() -> None:
    Path("20000101_first_start_to_first_end.gpx").unlink()
    Path("20000102_second_start_to_second_end.gpx").unlink()
