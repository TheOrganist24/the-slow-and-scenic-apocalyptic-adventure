# Issues

## **TASK** Automation for exporting planned route to phone
Once routes are planned, the routes should be able to be exported and viewed on a handheld device

* **Raised**: 2024-02-10 14:30
* **Completed**: 
  * [x] Design updated
  * [x] Unit test
  * [x] Code written
  * [ ] Refactored


## **TASK** Refactor overview and leg pages
Overview and leg pages make many DB calls, refactor to use PageBuilder class

* **Raised**: 2024-02-10 14:30
* **Completed**: 
  * [ ] Design updated
  * [ ] Unit test
  * [ ] Code written
  * [ ] Refactored


## **TASK** Test coverage is poor
Obtain full unittest coverage

* **Raised**: 2024-02-10 14:30
* **Completed**: 
  * [ ] Design updated
  * [ ] Unit test
  * [ ] Code written
  * [ ] Refactored


## **TASK** QGIS Planning Project
Need a QGIS project to connect to "upcoming hikes" table and plan out a route

* **Raised**: 2024-02-10 14:30
* **Completed**: 2024-02-10 23:00
  * [x] Design updated
  * [ ] Unit test
  * [x] Code written
  * [ ] Refactored


## **TASK** Logging
There is currently no logging.

* **Raised**: 2024-02-10 14:30
* **Completed**: 
  * [ ] Design updated
  * [ ] Unit test
  * [ ] Code written
  * [ ] Refactored


## **BUG** Missing template exception fails on deployemnt
The missing template validation fails due to the path being badly written

* **Raised**: 2024-02-10 14:30
* **Fixed**: 
  * [x] Failing test
  * [ ] Code to solve
